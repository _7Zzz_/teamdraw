import sys
from PyQt5 import QtWidgets

import ui_mainWindow
import mainWindow

def main():
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = mainWindow.MainWindow()
    ui = ui_mainWindow.Ui_MainWindow()
    ui.setupUi(MainWindow)
    #print("here")
    MainWindow.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
    
