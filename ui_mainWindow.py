# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'multyDraw.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import painter


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        self.sizeObject = QtWidgets.QDesktopWidget().screenGeometry(1)


        MainWindow.resize(self.sizeObject.width(), self.sizeObject.height())

        self.scribbleArea = painter.ScribbleArea()
        #self.setCentralWidget(self.scribbleArea)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButton = QtWidgets.QPushButton(self.scribbleArea)
        self.pushButton.setGeometry(QtCore.QRect(5, 10, 83, 25))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(self.scribbleArea)
        self.pushButton_2.setGeometry(QtCore.QRect(5, 45, 83, 25))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(self.scribbleArea)
        self.pushButton_3.setGeometry(QtCore.QRect(5, 80, 83, 25))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_4 = QtWidgets.QPushButton(self.scribbleArea)
        self.pushButton_4.setGeometry(QtCore.QRect(5, 115, 83, 25))
        self.pushButton_4.setObjectName("pushButton_4")
        MainWindow.setCentralWidget(self.scribbleArea)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 817, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton.setText(_translate("MainWindow", "PushButton"))
        self.pushButton_2.setText(_translate("MainWindow", "PushButton"))
        self.pushButton_3.setText(_translate("MainWindow", "PushButton"))
        self.pushButton_4.setText(_translate("MainWindow", "PushButton"))

